cwd            := $(shell pwd)
in_folder     := "src"
out_folder     := "$(HOME)/Dropbox/Carlo and SSJ/code/"

.PHONY: publish

publish::
	rm -rf $(out_folder) && mkdir $(out_folder) && cp -r $(in_folder)/* $(out_folder)
