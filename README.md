# Fake News Sars-CoV-2 model

## Description
A COVID SEIR-based model

- with detailed states (including # of hospitalized)
- taking into account real number of deaths
- estimating asymptomatic
- proposing [AASD](https://www.preprints.org/manuscript/202005.0005/v1)
