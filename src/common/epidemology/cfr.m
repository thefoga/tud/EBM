function r = cfr(died, confirmed)
% Calculate the case fatality rate
% Input
%
%   died: [1xN] double vector. Deaths over time
%   confirmed: [1xN] double vector. Confirmed over time
%
% Output
%
%   r: [1xN] double vector. Rate over time

r = died ./ confirmed;
end
