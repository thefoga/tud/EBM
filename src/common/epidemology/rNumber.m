function r = rNumber(infected, shift)
% Calculate Infected progression per delta time
%
% Input
%
%   Infected: [1xN] double vector
%   shift: [1x1] double. Shift
%
% Output
%
%   r: [1xN] double vector

r = zeros(1, length(infected) - shift);
for i = shift + 1 : length(infected)
    r(i) = infected(i) / infected(i - shift);
end
end
