function [Y] = RK4(fun, Y, dt)
% Input
%
%   fun: handle, parameters are (Y, A, F)
%   Y: vector [Nx1], current fun vector (Y(i))
%   dt: scalar [1x1], delta time to integrate on
% 
% Output
%
%   Y: scalar [Nx1], next fun vector (Y(i+1))

% Runge-Kutta of order 4
k_1 = fun(Y);
k_2 = fun(Y+0.5*dt*k_1);
k_3 = fun(Y+0.5*dt*k_2);
k_4 = fun(Y+k_3*dt);

% output
Y = Y + (1/6)*(k_1+2*k_2+2*k_3+k_4)*dt;
end