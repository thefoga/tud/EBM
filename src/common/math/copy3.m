function [yOut] = copy3(Y, n)
  yOut = Y;
  for i=1:n - 1
    yOut = cat(3, yOut, Y);
  end
end