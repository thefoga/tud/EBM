function [x] = extrapolateMinMax(x, extrapolationBefore, extrapolationAfter, isToExtrapolate)
%% Extrapolate values on min-max algorithm

if ~exist('isToExtrapolate', 'var')
  isToExtrapolate = @(x) isnan(x); % assume nan null-value
end

y = x;

% extrapolate time before
goOn = true;
i = 1;
while goOn
  if isToExtrapolate(x(i))
    x(i) = extrapolationBefore;
    i = i + 1;
  else % found a non null-value
    goOn = false;
  end
end

% extrapolate time after
goOn = true;
i = length(x);
while goOn
  if isToExtrapolate(x(i))
    x(i) = extrapolationAfter;
    i = i - 1;
  else % found a non null-value
    goOn = false;
  end
end
end