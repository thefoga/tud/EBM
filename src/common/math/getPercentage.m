function p = getPercentage(x, m, M)
% Input
%
%   x: [1xN] double vector. States over time
%   m: [1x1] double. Fixed value 0
%   M: [1x1] double -> nPop
%
% Output
%
%   p: [1xN] double vector with percentages over time

ratio = (x - m) / M;
p = ratio * 100.0;
end

