function [yOut] = sum3(Y)
  nRows = size(Y, 1);
  nCols = size(Y, 2);
  nDims = size(Y, 3);
  yOut = zeros(nRows, nCols);

  for k=1:nDims
    yOut = yOut + Y(:, :, k);
  end
end