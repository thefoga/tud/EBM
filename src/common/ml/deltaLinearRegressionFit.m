function [b] = deltaLinearRegressionFit(x, y, alpha)

dy = diff(y);

X = x(1:end - 1)';

[b, bint] = regress(dy', X, alpha); % solve multi-linear regression
b = [bint(1), b, bint(2)]; % lower CI, estimate, upper CI
end