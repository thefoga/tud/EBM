function [b] = deltaLinearRegressionFit(xs, y, alpha)

dy = diff(y);

X = xs(:, 1:end - 1)';

[b, bint] = regress(dy', X, alpha); % solve multi-linear regression
b = [bint(:, 1), b, bint(:, 2)]; % lower CI, estimate, upper CI
end
