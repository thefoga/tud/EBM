function [paramsFit, ci, sd] = fit(params, target, initState, mixingMatrix, popDist, dt, normalizer)

if ~exist('normalizer', 'var')
  normalizer = @(x) log(x);
end

%% unpack data
nPop = initState(1);
E0 = initState(2);
Iu0 = initState(3);
P0 = initState(4);
S0 = nPop - E0 - sum(target(:, 1)) - P0;
initState = [nPop; S0; E0; Iu0; target(:, 1); P0];

%% normalize data
for i=1:size(target, 1)
  target(i, :) = normalizer(target(i, :));
end

%% fitting function
fs = 1/dt;
time = (0:1:size(target, 2) - 1);
tTarget = round(time * fs) / fs;
t = tTarget(1):1:tTarget(end); % oversample to ensure that the algorithm converges

function [out] = optim(params, t0, initState, t)
  % Function we have to optimize

  %% unpack
  nPop = initState(1);
  initState = initState(2:end, :);

  %% Initial conditions
  N = numel(t);
  Y = getTimeVector(initState, N);

  %% ODE solution
  [Y] = simulate(params, Y, nPop, mixingMatrix, popDist, t);

  %% extract target
  outTmp = [];
  nPopClasses = size(outTmp, 1);
  for i=1:nPopClasses
      Du = Y(4, 1:N, i);
      Hm = Y(5, 1:N, i);
      Hs = Y(6, 1:N, i);
      Icu = Y(7, 1:N, i);
      R = Y(8, 1:N, i);
      D = Y(9, 1:N, i);

      outTmp = [outTmp; Du; Hm; Hs; Icu; R; D];
  end

  % oversample to ensure that the algorithm converges
  nStatesOut = size(outTmp, 1);
  out = zeros(nStatesOut, length(t0));
  for i=1:nStatesOut
    out(i, :) = interp1(t, outTmp(i, :), t0);
    out(i, :) = normalizer(out(i, :));
  end
end

f = @(params, t0) optim(params, t0, initState, t);

%% do fit
[paramsFit, ci, sd] = doFit(f, target, tTarget, initState, params);
end
