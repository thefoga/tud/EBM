function [A] = getLinearComp(params)
% Iteration matrix

nStates = 10;
A = zeros(nStates);

a = params(1);
c = params(3);
d = params(4);
e = params(5);
f = params(6);
g = params(7);
h = params(8);
i = params(9);
l = params(10);
m = params(11);
n = params(12);
o = params(13);
p = params(14);

A(1, 1) = -a; % S

A(2, 2) = -c; % E (-> Iu)

A(3, 2) = c; % Iu <- E
A(3, 3) = -(d + e + f + g);

A(4, 3) = g; % Du <- Iu

A(5, 3) = e; % Hm <- Iu 
A(5, 5) = -(h + i);

A(6, 3) = f; % Hs <- Iu
A(6, 5) = h; % Hs <- Hm
A(6, 6) = -(l + m + n);

A(7, 6) = m; % Icu <- Hs
A(7, 7) = -(o + p);

A(8, 5) = i; % R <- Hm
A(8, 6) = l; % R <- Hs
A(8, 7) = o; % R <- Icu

A(9, 6) = n; % D <- Hs
A(9, 7) = p; % D <- Icu

A(10, 1) = a; % P <- S
A(10, 3) = d; % P <- I
end