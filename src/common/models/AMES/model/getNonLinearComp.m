function [F] = getNonLinearComp(Y, params, nPop, mixingMatrix, k)
  % Y:
  % - rows are the model states
  % - just 1 column (last timestep)
  % - depth (3rd dimension) is the classes

  nStates = size(Y, 1); % # rows
  nClasses = size(mixingMatrix, 1); % ...or size(Y, 3)

  F = zeros(nStates, 1);
  S = Y(1, 1, k);

  I = zeros(1, nClasses);
  for c = 1:nClasses
    I(c) = Y(3, 1, c); % = # asymptomatic of c-th age-class on last timestep
  end
  I = I * mixingMatrix(k, :)'; % take into account the interaction -> "normalized" interacted scalar of infected across all age-strata

  Hm = sum(Y(5, 1, :));
  Hs = sum(Y(6, 1, :));
  Icu = sum(Y(7, 1, :));
  quarantined = Hm + Hs + Icu;

  Du = sum(Y(4, 1, :));
  D = sum(Y(8, 1, :));
  dead = Du + D;

  notInteracting = quarantined + dead;  % do not interact with others
  interacting = nPop - notInteracting;

  SI = S * I;

  b = params(2);
  interactionCoeff = b / interacting;

  F(1:2, 1) = [-interactionCoeff; interactionCoeff] .* SI;
end
