function [S, E, Iu, Du, Hm, Hs, Icu, R, D, P] = model(params, nPop, initState, mixingMatrix, popDist, t)
%% Simulate the time-stories of an epidemic outbreak using generalized SEIRD model
% Input
%
%   params: [1x14] double vector parameters
%   Npop:[1x1] double. Fixed Value = 60.48e6
%   E0: [1x1] double. E0 = initialLatent -> initialLatent = 0.02 * Npop
%   Iu0: [1x1] double. Iu0 = initialAsymp -> initialAsymp = 0.01 * Npop
%   Iq0: [1x1] double. Iq0 = TotPositive(1);
%   R0: [1x1] double. Initial Recovered
%   H0: [1x1] double. Initial Hospitalized
%   Icu: [1x1] double. Initial ICU
%   D0: [1x1] double. Initia Deaths
%   P0: [1x1] double. Initial Protected
%   t: [1xN] double vector. t = (0:N - 1).*dt;
%
% Output
%
%   S: [1xN] double vector. Susceptible over time
%   E: [1xN] double vector. Exposed over time
%   Iu: [1xN] double vector. Infected asymptomatic over time
%   Iq: [1xN] double vector. Infected quarantined over time
%   R: [1xN] double vector. Recovered over time
%   H: [1xN] double vector. Hospitalized over time
%   Icu: [1xN] double vector. ICU over time
%   D: [1xN] double vector. Deaths over time
%   P: [1xN] double vector. Protected over time

%% Initial conditions
N = numel(t);  % time steps
S0 = nPop - sum(initState);
initState = [S0; initState];
Y = getTimeVector(initState, N);

%% Matrix version of model
[Y] = simulate(params, Y, nPop, mixingMatrix, popDist, t);
if ndims(Y) > 2 % NOT allow for multi class output
  Y = sum3(Y);
end

%% Extract timeline
S = Y(1, :);
E = Y(2, :);
Iu = Y(3, :);
Du = Y(4, :);
Hm = Y(5, :);
Hs = Y(6, :);
Icu = Y(7, :);
R = Y(8, :);
D = Y(9, :);
P = Y(10, :);
end
