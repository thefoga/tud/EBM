function [] = doSimulation(regionName, plotTitle, getSettings, getAssumptions, saveVars)
%% Do simulations on AMES model

if ~exist('plotTitle', 'var')
  plotTitle = false;
end

if ~exist('saveVars', 'var')
  saveVars = false;
end

%% model setting
[hospitalizationData, endPredictionOn, doRobustnessAnalysis, mixingMatrix, popDist, dt] = getAssumptions();
endPredictionOn = datetime(endPredictionOn, 'InputFormat', 'yyyy-MM-dd');
nPop = hospitalizationData.nPop;
[params, E0, P0, fAsymp, undetectedDeaths, tFitStart, tFitEnd] = getSettings();

%% get data
userSpecifiedRegion = and(exist('regionName', 'var'), regionName ~= false);
if userSpecifiedRegion
  raw = getData(regionName);
else
  raw = getData();
end

[Iu_real, Du_real, Hm_real, Hs_real, Icu_real, R_real, D_real, Iq_real, time_real] = parseData(raw, tFitStart, tFitEnd, doRobustnessAnalysis, fAsymp, undetectedDeaths);

%% fitting
target = [Du_real; Hm_real; Hs_real; Icu_real; R_real; D_real];
Iu0 = Iu_real(1);
if E0 < 1  % user input a ratio
  E0 = E0 * nPop;
end

if P0 < 1  % user input a ratio
  P0 = P0 * nPop;
end
initState = [nPop, E0, Iu0, P0];

% entire population
[paramsFit, ci, sdFit] = fit(params, target, initState, [[1.0]], [1.0], dt);

%% simulate
time_fit = datetime(time_real(1)):dt:endPredictionOn;
N = numel(time_fit);
t_sample = (0:N - 1) * dt;

initState = [E0; Iu_real(1); Du_real(1); Hm_real(1); Hs_real(1); Icu_real(1); R_real(1); D_real(1); P0];
[S_fit, E_fit, Iu_fit, Du_fit, Hm_fit, Hs_fit, Icu_fit, R_fit, D_fit, P_fit] = model(paramsFit, nPop, initState, mixingMatrix, popDist, t_sample);

%% plot
data_fit = [Iu_real; Du_real; Hm_real; Hs_real; Icu_real; R_real; D_real; Iq_real];
time_fit = time_fit(1:1 / dt:end);
result_fit = [S_fit; E_fit; Iu_fit; Du_fit; Hm_fit; Hs_fit; Icu_fit; R_fit; D_fit; P_fit];
result_fit = result_fit(:, 1:1 / dt:end);
[Iu_real, Du_real, Hm_real, Hs_real, Icu_real, R_real, D_real, Iq_real, time_real_fut] = parseData(raw, tFitEnd, 0, 0, fAsymp, undetectedDeaths);
data_real = [Iu_real; Du_real; Hm_real; Hs_real; Icu_real; R_real; D_real; Iq_real];

error_fit = zeros(size(result_fit, 1), size(result_fit, 2)); % todo

if plotTitle
  plotResults(nPop, ...  
    result_fit, ...
    error_fit(:, 1:1 / dt:end), ...
    time_fit, ...
    data_fit, ...
    time_real, ...
    data_real, ...
    time_real_fut, ...
    plotTitle...
  );
end

%% show error of simulation
labels = {'dead (undetected)', 'home confined', 'hospitalized', 'ICU', 'recovered', 'dead'};
summaryErrors(...
  labels, ...
  [Du_fit; Hm_fit; Hs_fit; Icu_fit; R_fit; D_fit], ...
  target, ...
  dt ...
);

%% summary
summary(paramsFit, ci, sdFit, params);

%% save variables
if saveVars
  save(saveVars);
end

end