function [Y] = simulate(params, Y, nPop, mixingMatrix, popDist, t)
iteration = @(Y, A, F) A * Y + F;

A = getLinearComp(params);
mixingMatrix = mixingMatrix .* 2 - eye(size(mixingMatrix, 1));  % linear transform
nClasses = size(mixingMatrix, 1);

YC = copy3(Y, nClasses);
% distribute initial population
for k=1:nClasses
  YC(:, :, k) = YC(:, :, k) * popDist(k);
end

% YC:
% - rows are the states within 1 class at a timestep
% - columns are the timesteps
% e.g a 3-state n-time (showing just 1 class):
%     - - - ... - - - - - - -
%     - - - ... - - - - - - -
%     - - - ... - - - - - - -
% - depth (3rd dimension) is the classes

N = numel(t);
dt = median(diff(t));
for i=2:N % for each timestep
  for k=1:nClasses % for each class -> do its own evolution
    F = getNonLinearComp(YC(:, i - 1, :), params, nPop, mixingMatrix, k);
    fun = @(Y) iteration(Y, A, F);
    YC(:, i, k) = RK4(fun, YC(:, i - 1, k), dt);
    % todo add threshold if < 1 -> 0
  end
end

Y = YC; % return (possibly) each class
end
