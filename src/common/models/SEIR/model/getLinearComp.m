function [A] = getLinearComp(params)
% Iteration matrix

nStates = 4;
A = zeros(nStates);

a = params(1);
b = params(2);
c = params(3);

A(1, 1) = -a; % S (-> E)

A(2, 1) = a; % E (<- S)
A(2, 2) = -b; % E (-> I)

A(3, 2) = b; % I (<- E)
A(3, 3) = -c; % I (-> R)

A(4, 3) = c; % R (<- I)
end