function [Y] = simulate(params, Y, nPop, t)
iteration = @(Y, A) A * Y;

A = getLinearComp(params);

N = numel(t);
dt = median(diff(t));
for i=2:N % for each timestep
  YC(:, i) = RK4(iteration, Y(:, i - 1), dt);
  % todo add threshold if < 1 -> 0
end
end