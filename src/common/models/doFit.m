function [paramsFit, ci, sd] = doFit(f, target, tTarget, initState, params)
%% Options for lsqcurvfit
options = optimset(...
    'TolX', 1e-5, ...
    'TolFun', 1e-3, ...
    'MaxFunEvals', 1e4, ...
    'MaxIter', 1e2, ...
    'Display', 'iter-detailed'...
);

%% bounds
lowerBounds = params(:, 1);
guess = params(:, 2);
upperBounds = params(:, 3);

%% fit
[paramsFit, ~, residual, ~, ~, ~, J] = lsqcurvefit(f, guess, tTarget(:)', target, lowerBounds, upperBounds, options);

ci = nlparci(paramsFit, residual, 'jacobian', J); % 95 % CI
sd = getStd(paramsFit, ci);
end