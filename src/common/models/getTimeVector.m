function [Y] = getTimeVector(v, n)
% Iteration vector

nStates = length(v);
Y = zeros(nStates, n);
Y(:, 1) = v;
end