function [xWithError] = addGaussianError(x, sd)
% Add an error

xWithError = x; % same shape

for i = 1:length(x)
    xWithError(i) = normrnd(x(i), sd(i));
end
end
