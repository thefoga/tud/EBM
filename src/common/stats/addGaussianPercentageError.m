function [xWithError] = addGaussianPercentageError(x, stdPercentage)
% Add an error to read values
% Input
%
%   x: [1x22] double vector. State value read from table over time
%   stdPercentage: errorStd -> errorStd = doRobustnessAnalysis = 2;
%
% Output
%
%   xWithError: [1x22] double vector. State value read from table over time + errorStd

xWithError = zeros(size(x, 1), size(x, 2));

for i = 1:length(x)
  errorPercentage = normrnd(0, stdPercentage / 100);
  xWithError(i) = x(i) * (1 + errorPercentage);
end

end

