function [s] = getStd(yFit, yCI)
%% Get Standard deviation
% Input
%
%    yFit: [1x14] double vector -> paramsFit
%    yCI: [14x2] double matrix confidence interval (95%)
%
% Output
%
%    [s]: [1xN] double vector

cdf = @(x, u, s) 1/2 * (1 + erf((x - u) / (s * sqrt(2))));
pInside = @(xMax, xMin, u, s) cdf(xMax, u, s) - cdf(xMin, u, s);
within2Std = @(u, s) pInside(u + 2 * s, u - 2 * s, u, s); % witihn 2 STD (~0.95, 95 %)

n = length(yFit);
s = zeros(1, n);  % horizontal vector

for i=1:n
  u = yFit(i);
  CIu = yCI(i, 2);
  CIl = yCI(i, 1);

  f = @(s) pInside(CIu, CIl, u, s) - within2Std(u, s); % function we want to minimize
  x0 = (CIu - CIl) / 4;
  sd = fzero(f, x0);
    
  s(i) = sd;
end
end

