function [rmse, nrmse] = mof(realData, yourPrediction)
%% Measures of error. 
% Input
%
%   realData: [1xN] double vector. Value from the table (totPositive, recovered, hospitalized)
%   yourPrediction: [1xN] double double vector. Predicted values over time  
%
% Output
%
%   rmse: [1x1] double. Root mean square error
%   nrmse: [1x1] double. Normalized root mean square error

%% parse
n = min(length(realData), length(yourPrediction));
realData = realData(1:n);
yourPrediction = yourPrediction(1:n);

%% MSE
mse = mean((realData - yourPrediction).^2);

%% RMSE
rmse = sqrt(mse);

%% Normalized RMSE
nrmse = rmse / mean(realData);
end

