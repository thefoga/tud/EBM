function [s] = stdFromCi(ci)
  s = (ci(2) - ci(1)) / 4;
end

