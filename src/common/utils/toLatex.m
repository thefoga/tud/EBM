function [] = toLatex(paramsFit, sdFit, originalParams, paramsNames, daysParams)
formatter = '%s & %s & %f & %f & %f & %f & %f \\\\ \\hline\n';
formatterDay = '\\frac{1}{%s} & %s & %f & %f & %f & %f & %f \\\\ \\hline\n';

for i = 1:length(paramsFit)
  paramName = paramsNames(i);
  if ismember(paramName, daysParams)
    fprintf(formatterDay, ...
      paramName, ...
      '', ...
      1 / paramsFit(i), ...
      1 / originalParams(i, 3), ...
      1 / originalParams(i, 2), ...
      1 / originalParams(i, 1), ...
      sdFit(i) ...
    );
  else
    fprintf(formatter, ...
      paramName, ...
      '', ...
      paramsFit(i), ...
      originalParams(i, 1), ...
      originalParams(i, 2), ...
      originalParams(i, 3), ...
      sdFit(i) ...
    );
  end
end
end