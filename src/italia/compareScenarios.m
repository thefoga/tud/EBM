clearvars; close all; clc;

addpath('../common/models/');
addpath('../common/models/AMES/');
addpath('../common/models/AMES/model');
addpath('../common/models/AMES/sim');
addpath('../common/models/AMES/fitting');
addpath('../common/stats/');
addpath('../common/math/');
addpath('../common/epidemology/');
addpath('../common/utils');
addpath('../italia/utils');
addpath('./scenarios');

%% get data
[timeReference, tStartLockdown, tEndLockdown, tTourismStart, ~] = getLockdownTimeline(); % cache

%% do simulations
doSimulation(false, false, @getBELockdownSettings, 'BELockdown.mat');
doSimulation(false, false, @getBESocialDistancingSettings, 'BESocialDistancing.mat');
doSimulation(false, false, @getBENoRestrictionsSettings, 'BENoRestrictions.mat');
doSimulation(false, false, @getOPSocialDistancingSettings, 'OPSocialDistancing.mat');
doSimulation(false, false, @getOPNoRestrictionsSettings, 'OPNoRestrictions.mat');

%% beginning of epidemic: lockdown, social distancing, no restrictions
figure
sgtitle('FN model for Sars-CoV-2 in Italy on beginning of epidemic');

%% asymptomatic
subplot(5, 1, 1);
showTimeline(timeReference, tStartLockdown, tEndLockdown, tTourismStart); hold on
plotBeginningEpidemicData(3, -1);
title('Asymptomatic (undetected)');

%% hospitalized (light, not ICU)
subplot(5, 1, 2);
showTimeline(timeReference, tStartLockdown, tEndLockdown, tTourismStart); hold on
plotBeginningEpidemicData(6, 4);
title('Hospitalized (light, not ICU)');

%% ICU
subplot(5, 1, 3);
showTimeline(timeReference, tStartLockdown, tEndLockdown, tTourismStart); hold on
plotBeginningEpidemicData(7, 5);
title('ICU');

%% recovered
subplot(5, 1, 4);
showTimeline(timeReference, tStartLockdown, tEndLockdown, tTourismStart); hold on
plotBeginningEpidemicData(8, 6);
title('Total recovered');

%% dead
subplot(5, 1, 5);
showTimeline(timeReference, tStartLockdown, tEndLockdown, tTourismStart); hold on

load('BELockdown.mat');
col = 'r';
p0 = plot(time_fit, result_fit(4, :) + result_fit(9, :), strcat('-', col)); hold on
plot(time_real, data_fit(2, :) + data_fit(7, :), strcat('x', col)); hold on

load('BESocialDistancing.mat');
col = 'g';
p1 = plot(time_fit, result_fit(4, :) + result_fit(9, :), strcat('-', col)); hold on
plot(time_real, data_fit(2, :) + data_fit(7, :), strcat('^', col)); hold on

load('BENoRestrictions.mat');
col = 'b';
p2 = plot(time_fit, result_fit(4, :) + result_fit(9, :), strcat('-', col)); hold on
plot(time_real, data_fit(2, :) + data_fit(7, :), strcat('v', col)); hold on

leg = { ...
'lockdown', ...
'social distancing', ...
'no restrictions' ...
};
legend([p0, p1, p2], leg{:});

title('Total dead (undetected too)');

%% opening phase: social distancing, no restrictions
figure
sgtitle('FN model for Sars-CoV-2 in Italy on opening phase');

%% asymptomatic
subplot(5, 1, 1);
showTimeline(timeReference, tStartLockdown, tEndLockdown, tTourismStart); hold on
plotOpeningPhaseData(3, -1);
title('Asymptomatic (undetected)');

%% hospitalized (light, not ICU)
subplot(5, 1, 2);
showTimeline(timeReference, tStartLockdown, tEndLockdown, tTourismStart); hold on
plotOpeningPhaseData(6, 4);
title('Hospitalized (light, not ICU)');

%% ICU
subplot(5, 1, 3);
showTimeline(timeReference, tStartLockdown, tEndLockdown, tTourismStart); hold on
plotOpeningPhaseData(7, 5);
title('ICU');

%% recovered
subplot(5, 1, 4);
showTimeline(timeReference, tStartLockdown, tEndLockdown, tTourismStart); hold on
plotOpeningPhaseData(8, 6);
title('Total recovered');

%% dead
subplot(5, 1, 5);
showTimeline(timeReference, tStartLockdown, tEndLockdown, tTourismStart); hold on

load('OPSocialDistancing.mat');
col = 'g';
p0 = plot(time_fit, result_fit(4, :) + result_fit(9, :), strcat('-', col)); hold on
plot(time_real, data_fit(2, :) + data_fit(7, :), strcat('^', col)); hold on

load('OPNoRestrictions.mat');
col = 'b';
p1 = plot(time_fit, result_fit(4, :) + result_fit(9, :), strcat('-', col)); hold on
plot(time_real, data_fit(2, :) + data_fit(7, :), strcat('v', col)); hold on

leg = { ...
'social distancing', ...
'no restrictions' ...
};
legend([p0, p1], leg{:});

title('Total dead (undetected too)');

%% helpers

function [] = plotVertical(x, time_ref, hexColor)
  xline(time_ref(x), 'Color', hexColor);
end

function [] = showTimeline(timeReference, tStartLockdown, tEndLockdown, tTourismStart)
  plotVertical(tStartLockdown, timeReference, '#ffd8ff');
  plotVertical(tEndLockdown, timeReference, '#ff00ff');
  plotVertical(tTourismStart, timeReference, '#3b003b');
end

function [] = plotBeginningEpidemicData(fitI, realI)
  load('BELockdown.mat');
  col = 'r'; style = '-';
  p0 = plot(time_fit, result_fit(fitI, :), strcat(style, col)); hold on
  if realI > 0
    style = 'x'; 
    plot(time_real, data_fit(realI, :), strcat(style, col)); hold on
  end

  load('BESocialDistancing.mat');
  col = 'g'; style = '-';
  p1 = plot(time_fit, result_fit(fitI, :), strcat(style, col)); hold on
  if realI > 0
    style = '^';
    plot(time_real, data_fit(realI, :), strcat(style, col)); hold on
  end

  load('BENoRestrictions.mat');
  col = 'b'; style = '-';
  p2 = plot(time_fit, result_fit(fitI, :), strcat(style, col)); hold on
  if realI > 0
    style = 'v';
    plot(time_real, data_fit(realI, :), strcat(style, col)); hold on
  end

  leg = { ...
    'lockdown', ...
    'social distancing', ...
    'no restrictions' ...
  };
  legend([p0, p1, p2], leg{:});
end

function [] = plotOpeningPhaseData(fitI, realI)
  load('OPSocialDistancing.mat');
  col = 'g'; style = '-';
  p0 = plot(time_fit, result_fit(fitI, :), strcat(style, col)); hold on
  if realI > 0
     style = '^';
    plot(time_real, data_fit(realI, :), strcat(style, col)); hold on
  end

  load('OPNoRestrictions.mat');
  col = 'b'; style = '-';
  p1 = plot(time_fit, result_fit(fitI, :), strcat(style, col)); hold on
  if realI > 0
     style = 'v';
    plot(time_real, data_fit(realI, :), strcat(style, col)); hold on
  end

  leg = { ...
    'social distancing', ...
    'no restrictions' ...
  };
  legend([p0, p1], leg{:});
end
