function [hospitalizationData, endPredictionOn, doRobustnessAnalysis, mixingMatrix, popDist, dt] = getAASDAssumptions()

%% hospitalization
hospitalizationData = getHospitalizationData();

%% predictions
endPredictionOn = '2020-10-01';
doRobustnessAnalysis = 2;
dt = 1/24;

% This is NOT a symmetric matrix! Each row expresses the interaction between the class at that row and the others
% e.g
% mixingMatrix =
% 0.6000    0.3000    0.1000
% 0.5000    0.5000         0
% 0.1000    0.2000    0.7000
% => class 2 does NOT interact with class 3
% => class 1 interacts with class 2 interaction with 0.3000
% => the interactions of class 1 are 1/10 of the total possible with class 1
% => the interactions of class 1 are 2/10 of the total possible with class 2
% => the interactions of class 1 are 7/10 of the total possible with class 3
% THE VALUES MUST BE BETWEEN 0 (no interaction) AND 1 (all people from one class interact with all people from the other) !
% e.g in a totally mixed society (everyone interacts with everybody else) the matrix is the identity (all 1s)
% e.g if members of society are all self-isolated the matrix consists of all 0s

mixingMatrix = [
  [0.01];
];

popDist = [1.0]; % sum should be 1

end