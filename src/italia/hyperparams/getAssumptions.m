function [hospitalizationData, endPredictionOn, doRobustnessAnalysis, mixingMatrix, dt] = getAssumptions()

%% hospitalization
hospitalizationData = getHospitalizationData();

%% predictions
endPredictionOn = '2020-08-01';
doRobustnessAnalysis = 2;
dt = 1/24;

mixingMatrix = [
  [1];
];

end