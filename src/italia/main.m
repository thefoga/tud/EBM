clearvars; close all; clc;

addpath('../common/models/');
addpath('../common/models/AMES/');
addpath('../common/models/AMES/model');
addpath('../common/models/AMES/sim');
addpath('../common/models/AMES/fitting');
addpath('../common/stats/');
addpath('../common/math/');
addpath('../common/epidemology/');
addpath('../common/utils');
addpath('./utils');
addpath('./scenarios');
addpath('./hyperparams');

plotTitle = 'FN model for Sars-CoV-2 in Italy on beginning phase: lockdown';
getSettings = @getBEAASDSettings;
getAssumptions = @getAASDAssumptions;
doSimulation(false, plotTitle, getSettings, getAssumptions, 'BELockdownDistancing');
