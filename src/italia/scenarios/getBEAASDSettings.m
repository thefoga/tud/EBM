function [params, E0, P0, fAsymp, undetectedDeaths, tFitStart, tFitEnd] = getBELockdownSettings()
% Settings
%
% Output
%
%   params: [14x3] min, guess, max param estimate
%   E0: [1x1] initial exposed (% of total initial population)
%   P0: [1xN] initial protected (% of total initial population)
%   fAsymp: [1x1] fraction of asymptomatic (undetected) (% of total positive)
%   undetectedDeaths: [1x1] undetected deaths (% on the confirmed deaths)
%   tFitStart: [1x1] datetime (time to start fitting process)
%   tFitEnd: [1x1] datetime (time to end fitting process)

params = [ ...
  [0.02, 0.04, 0.1]; ... % a
  [0.36, 0.4, 0.5]; ... % b
  [1/14, 1/10, 1/8]; ... % c
  [1/21, 1/14, 1/7]; ... % d
  [0.007, 0.03, 0.04]; ... % e
  [0.007, 0.01, 0.02]; ... % f
  [0.0006, 0.0018, 0.0020]; ... % g
  [0.002, 0.010, 0.020]; ... % h
  [0.004, 0.010, 0.020]; ... % i
  [1/50, 1/30, 1/20]; ... % l
  [0.010, 0.020, 0.050]; ... % m
  [0.001, 0.01, 0.05]; ... % n
  [1/20, 1/4, 1/2]; ... % o
  [0.001, 0.01, 0.05]... % p
];

%% fake news
undetectedDeaths = 0.4; % see latest INPS report
fAsymp = 0.432;
E0 = 0.035;
P0 = 0.5;

[~, tStartLockdown, tEndLockdown, tTourismStart, offset] = getLockdownTimeline();
tFitStart = tStartLockdown + offset;
tFitEnd = tEndLockdown - offset;

end