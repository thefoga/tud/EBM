function [params, E0, P0, fAsymp, undetectedDeaths, tFitStart, tFitEnd] = getBESocialDistancingSettings()
% Settings
%
% Output
%
%   params: [14x3] min, guess, max param estimate
%   E0: [1x1] initial exposed (% of total initial population)
%   P0: [1xN] initial protected (% of total initial population)
%   fAsymp: [1x1] fraction of asymptomatic (undetected) (% of total positive)
%   undetectedDeaths: [1x1] undetected deaths (% on the confirmed deaths)
%   tFitStart: [1x1] datetime (time to start fitting process)
%   tFitEnd: [1x1] datetime (time to end fitting process)

params = [ ...
  [0.01, 0.05, 0.1]; ... % a
  [0.75, 0.9, 1]; ... % b
  [1/15, 1/12, 1/7]; ... % c
  [1/21, 1/14, 1/7]; ... % d
  [0.0020, 0.0025, 0.0030]; ... % e
  [0.009, 0.0095, 0.010]; ... % f
  [0.0016, 0.0018, 0.0020]; ... % g
  [0.008, 0.009, 0.010]; ... % h
  [0.012, 0.015, 0.018]; ... % i
  [0.040, 0.045, 0.050]; ... % l
  [0.010, 0.020, 0.030]; ... % m
  [0.001, 0.01, 0.02]; ... % n
  [0.05, 0.10, 0.50]; ... % o
  [0.001, 0.01, 0.02]... % p
];

%% fake news
undetectedDeaths = 0.4; % see latest INPS report
fAsymp = 0.432;
E0 = 0.035;
P0 = 0.1;

[~, tStartLockdown, tEndLockdown, tTourismStart, offset] = getLockdownTimeline();
tFitStart = 1 + offset;
tFitEnd = tStartLockdown - offset;

end