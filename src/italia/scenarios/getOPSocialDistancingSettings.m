function [params, E0, P0, fAsymp, undetectedDeaths, tFitStart, tFitEnd] = getOPSocialDistancingSettings()

% Settings
%
% Output
%
%   params: [14x3] min, guess, max param estimate
%   E0: [1x1] initial exposed (% of total initial population)
%   P0: [1xN] initial protected (% of total initial population)
%   fAsymp: [1x1] fraction of asymptomatic (undetected) (% of total positive)
%   undetectedDeaths: [1x1] undetected deaths (% on the confirmed deaths)
%   tFitStart: [1x1] datetime (time to start fitting process)
%   tFitEnd: [1x1] datetime (time to end fitting process)

m = 1e-5;
M = 1 - m;
params = [ ...
  [0.5, 0.7, M]; ... % a
  [m, 1e-3, 1e-2]; ... % b
  [1/14, 1/13, 1/12]; ... % c
  [1/14, 1/12, 1/10]; ... % d
  [m, 0.05, M]; ... % e
  [m, 0.005, 0.01]; ... % f
  [m, 0.0025, 0.0040]; ... % g
  [m, 0.05, 0.1]; ... % h
  [m, 0.05, 0.1]; ... % i
  [1/28, 1/21, 1/14]; ... % l
  [m, 0.01, 0.02]; ... % m
  [m, 0.01, 0.02]; ... % n
  [1/18, 1/15, 1/12];... % o
  [0.1, 0.2, 0.3];... % p
];

%% fake news
undetectedDeaths = 0.1;
fAsymp = 0.2;
E0 = 51000;
P0 = 0.9;

[~, tStartLockdown, tEndLockdown, tTourismStart, offset] = getLockdownTimeline();
tFitStart = tEndLockdown + offset;
tFitEnd = tTourismStart - offset;

end