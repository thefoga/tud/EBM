function table = getData(regionName, dataFile, fileURL)
% Collects the updated data of the COVID-19 pandemic in Italy from the
% Italian governement (https://github.com/pcm-dpc/COVID-19)

if ~exist('regionName', 'var')
  regionName = false;
end

if ~exist('dataFile', 'var')
  dataFile = '../italia/data/data.csv';
  dataFile = fullfile(pwd, dataFile);
end

if ~exist('fileUrl', 'var')
  fileURL = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni.csv';
end

%% Setup the Import Options and import the data
% Set the number of columns
opts = delimitedTextImportOptions("NumVariables", 16);

% Specify range and delimiter
dataLines = [2, Inf];
opts.DataLines = dataLines;
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["Date", "_", "RegionCode", "RegionName", "_", "_", "inHospitalWithSymptoms", "inHospitalICU", "totInHospital", "homeConfinement", "totPositive", "_", "_", "recovered", "dead", "totCases", "test"];
opts.VariableTypes = ["string", "string" , "uint8" , "string" , "double", "double", "double" , "double" , "double" , "double" , "double" , "double" , "double" , "double" , "double", "double" , "double"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
opts.PreserveVariableNames = true;

% download the CSV file
% uncomment to update data:websave(dataFile, fileURL);

% import the data
fid = fopen(dataFile);
table = readtable(dataFile, opts);
fclose(fid);

time = unique(datetime(datestr(datenum(table.Date, 'yyyy-mm-DDThh:MM:ss'))));
fprintf(['Data updated on ', datestr(time(end)), '\n'])  % debug only

if regionName
  table = table(table.RegionName == regionName, :); % extract by region
else
  tableAcc = varfun(@sum, table, 'InputVariables', table.Properties.VariableNames(7:end), 'GroupingVariables','Date'); % merge regional data for each day

  % remove the 'GroupCount' variable (= 19 + 2 autonomous provinces)
  tableAcc = removevars(tableAcc, 'GroupCount');

  % rename the accumulated variables with the original variable names
  tableAcc.Properties.VariableNames = [table.Properties.VariableNames(1), table.Properties.VariableNames(7:end)];

  table = tableAcc;
end
end
