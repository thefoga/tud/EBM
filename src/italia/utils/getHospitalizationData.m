function table = getHospitalizationData(regionName)
% Collects the hospitalization data

if ~exist('dataFile', 'var')
  dataFile = '../italia/data/hospitalization.csv';
  dataFile = fullfile(pwd, dataFile);
end

if ~exist('regionName', 'var')
  regionName = false;
end

dataLines = [2, Inf];

%% Setup the Import Options and import the data
% Set the number of columns
opts = delimitedTextImportOptions("NumVariables", 4);

% Specify range and delimiter
opts.DataLines = dataLines;
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["RegionName", "nPop", "onStart", "onEnd"];
opts.VariableTypes = ["string", "double", "double" , "double"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
opts.PreserveVariableNames = true;

% import the data
fid = fopen(dataFile);
table = readtable(dataFile, opts);
fclose(fid);

if regionName
  table = table(table.RegionName == regionName, :); % extract by region
  table = removevars(table, 'RegionName');
else
  tableAcc = varfun(@sum, table, 'InputVariables', table.Properties.VariableNames(2:end));
  tableAcc.Properties.VariableNames = table.Properties.VariableNames(2:end); % without 'sum_'
  table = tableAcc;
end
end
