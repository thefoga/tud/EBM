function [timeReference, tStartLockdown, tEndLockdown, tTourismStart, offset] = getLockdownTimeline()

data = getData();
timeReference = unique(datetime(datestr(datenum(data.Date, 'yyyy-mm-DDThh:MM:ss'))));

%% lockdown timeline
tStartLockdown = 15; % 2020-03-09
tEndLockdown = 71; % 2020-05-04
tTourismStart = 101; % 2020-06-03

%% fitting settings
offset = 3; % allow for some days more

end