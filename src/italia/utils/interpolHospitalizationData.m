function [hospitalizationLevels] = interpolHospitalizationData(data, tSample)

hospitalizationLevelStart = data.onStart;
hospitalizationLevelEnd = data.onEnd;
hospitalizationLevelTime = [
  datetime('2020-02-24 17:00:00', 'InputFormat', 'yyyy-MM-dd HH:mm:ss'), ...
  datetime('2020-04-29 17:00:00', 'InputFormat', 'yyyy-MM-dd HH:mm:ss') ...
];

yKnown = [hospitalizationLevelStart, hospitalizationLevelEnd];

% linear interpolation
hospitalizationLevels = interp1(hospitalizationLevelTime, yKnown, tSample, 'linear', nan);
hospitalizationLevels = extrapolateMinMax(hospitalizationLevels, hospitalizationLevelStart, hospitalizationLevelEnd);