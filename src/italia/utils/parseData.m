function [Iu, Du, Hm, Hs, Icu, R, D, Iq, time] = parseData(raw, tStart, tEnd, errorStd, fAsymp, undetectedDeaths)
if tEnd ~= 0
  data = raw(tStart:tEnd, :);
else
  data = raw(tStart:end, :); % until the end
end


Hm = data.homeConfinement';
Hs = data.inHospitalWithSymptoms';
Icu = data.inHospitalICU';
R = data.recovered';
D = data.dead';
Iq = data.totPositive';

if errorStd ~= 0
  Iq = addGaussianPercentageError(Iq, errorStd);
  Hs = addGaussianPercentageError(Hs, errorStd);
  Icu = addGaussianPercentageError(Icu, errorStd);
  Hm = addGaussianPercentageError(Hm, errorStd);
  R = addGaussianPercentageError(R, errorStd);
  D = addGaussianPercentageError(D, errorStd);
end

%% fake news data
fAsymp = 1 / (1 - fAsymp); % solving asymp = f% * (asymp + confirmed)
Iu = Iq * fAsymp; % asymptomatic
Du = D * undetectedDeaths; % undetected deaths

% extract time
time = unique(datetime(datestr(datenum(data.Date, 'yyyy-mm-DDThh:MM:ss'))));
end