function [] = plotResults(nPop, result_pred, error_pred, time_pred, data_fit, time_fit, data_real, time_real, plotTitle)

%% unpack data
S_pred = result_pred(1, :); S_err = error_pred(1, :);
E_pred = result_pred(2, :); E_err = error_pred(2, :);
Iu_pred = result_pred(3, :); Iu_err = error_pred(3, :);
Du_pred = result_pred(4, :); Du_err = error_pred(4, :);
Hm_pred = result_pred(5, :); Hm_err = error_pred(5, :);
Hs_pred = result_pred(6, :); Hs_err = error_pred(6, :);
Icu_pred = result_pred(7, :); Icu_err = error_pred(7, :);
R_pred = result_pred(8, :); R_err = error_pred(8, :);
D_pred = result_pred(9, :); D_err = error_pred(9, :);
P_pred = result_pred(10, :); P_err = error_pred(10, :);
Iq_pred = Hm_pred + Hs_pred + Icu_pred; Iq_err = mean([Hm_err; Hs_err; Icu_err]);
deaths_pred = Du_pred + D_pred; deaths_err = mean([Du_err; D_err]);

Iu_fit = data_fit(1, :);
Du_fit = data_fit(2, :);
Hm_fit = data_fit(3, :);
Hs_fit = data_fit(4, :);
Icu_fit = data_fit(5, :);
R_fit = data_fit(6, :);
D_fit = data_fit(7, :);
Iq_fit = data_fit(8, :);
deaths_fit = Du_fit + D_fit;

Iu_real = data_real(1, :);
Du_real = data_real(2, :);
Hm_real = data_real(3, :);
Hs_real = data_real(4, :);
Icu_real = data_real(5, :);
R_real = data_real(6, :);
D_real = data_real(7, :);
Iq_real = data_real(8, :);
deaths_real = Du_real + D_real;

%% get timeline
function [] = showTimeline()
  [timeReference, tStartLockdown, tEndLockdown, tTourismStart, ~] = getLockdownTimeline();

  plotVertical(tStartLockdown, timeReference, '#ffd8ff');
  plotVertical(tEndLockdown, timeReference, '#ff00ff');
  plotVertical(tTourismStart, timeReference, '#3b003b');
end

fig = figure;

% tick labels
dateFormat = 'mmm dd';

subplot(2, 1, 1) % top

plotTimeline = @(t, y, col) plot(t, y, col);
plotVertical = @(x, time_ref, hexColor) xline(time_ref(x), 'Color', hexColor);
plotPred = @(y, col) plotTimeline(time_pred, y, col);  % predicted
plotFit = @(y, col) plotTimeline(time_fit, y, strcat('x', col));  % data used for fitting
plotReal = @(y, col) plotTimeline(time_real, y, strcat('o', col));  % other real data

plotPred(Hm_pred,'r'); hold on
plotPred(Hs_pred,'b'); hold on
plotPred(Icu_pred, 'm'); hold on
plotPred(R_pred, 'g'); hold on
plotPred(deaths_pred, 'k'); hold on

plotFit(Hm_fit, 'r'); hold on
plotFit(Hs_fit, 'b'); hold on
plotFit(Icu_fit, 'm'); hold on
plotFit(R_fit, 'g'); hold on
plotFit(deaths_fit, 'k'); hold on

plotReal(Hm_real, 'r'); hold on
plotReal(Hs_real, 'b'); hold on
plotReal(Icu_real, 'm'); hold on
plotReal(R_real, 'g'); hold on
plotReal(deaths_real, 'k'); hold on

showTimeline();

legend( ...
  'C + H + ICU (95% CI)', ...
  'H (95% CI)', ...
  'ICU (95% CI)', ...
  'total R (95% CI)', ...
  'total D + Du (95% CI)', ...
  'fit C + H + ICU', ...
  'fit H', ...
  'fit ICU', ...
  'fit R', ...
  'fit total D + Du', ...
  'real C + H + ICU', ...
  'real H', ...
  'real ICU', ...
  'real R', ...
  'real total D + Du', ...
  'start lockdown', ...
  'end lockdown', ...
  'borders open' ...
);
datetick(gca, 'x', dateFormat, 'keepticks');

subplot(2, 1, 2) % bottom

plotPred(E_pred, '--c'); hold on
plotPred(Iu_pred, '--b'); hold on
showTimeline();

legend( ...
  'E', ...
  'I', ...
  'start lockdown', ...
  'end lockdown', ...
  'borders open' ...
);
datetick(gca, 'x', dateFormat, 'keepticks');

% prettify
han = axes(fig, 'visible', 'off');
han.Title.Visible = 'on';
han.XLabel.Visible = 'on';
han.YLabel.Visible = 'on';

ylabel(han, 'population');
xlabel(han, 'time (days)');
title(han, plotTitle);

end