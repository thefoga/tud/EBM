function [] = summary(paramsFit, ci, sdFit, originalParams)
% Input
%
%   paramsFit: [1x14] double vector
%   nrmse:[1x5] double vector. Normalized root mean square error
%   sd: [1x14] double vector. Standard deviation
%   exposedPrediction: [1xN] double vector
%   timePrediction: [1xN] datetime

alphabet = 'abcdefghilmnopqrstuvwyz';
formatter = '%s fit ~ N(%f, %f^2). Initial guess was [%f, %f]\n';
daysParams = ['c', 'd', 'l', 'o']; % index of params that are meaningful in days
for i = 1:length(paramsFit)
  paramLetter = alphabet(i);
  if ismember(paramLetter, daysParams)
    f = strcat('1/', formatter);
    fprintf(f, paramLetter, 1 / paramsFit(i), sdFit(i), 1 / originalParams(i, 3), 1 / originalParams(i, 1));
  else
    fprintf(formatter, paramLetter, paramsFit(i), sdFit(i), originalParams(i, 1), originalParams(i, 3));
  end
end
fprintf('\n');

% toLatex(paramsFit, sdFit, originalParams, alphabet, daysParams);
% fprintf('\n');
end