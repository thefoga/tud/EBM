function [] = summaryErrors(labels, predictions, realData, dt)

n = numel(labels);
totRMSE = 0;
totNRMSE = 0;

for i=1:n
  label = string(labels(i));
  prediction = predictions(i, :);
  prediction(1:1/dt:length(prediction)); % rescale with time
  realValue = realData(i, :);

  [rmse, nrmse] = mof(realValue, prediction);
  totRMSE = totRMSE + rmse;
  totNRMSE = totNRMSE + nrmse;

  fprintf('%s RMSE: %f, NRMSE: %f\n', label, rmse, nrmse);
end

fprintf('avg RMSE: %f, avg NRMSE: %f\n', totRMSE / n, totNRMSE / n);
fprintf('\n');

end