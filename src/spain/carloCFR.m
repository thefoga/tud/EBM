clearvars; close all; clc;

addpath('../common/models/');
addpath('../common/models/FNH/');
addpath('../common/models/FNH/model');
addpath('../common/models/FNH/sim');
addpath('../common/models/FNH/fitting');
addpath('../common/stats/');
addpath('../common/math/');
addpath('../common/epidemology/');
addpath('../common/utils');
addpath('./utils');
addpath('./scenarios');

fig = figure;

% CFR
% real
[c, ~] = getTimelineFromSheet('./data/female.xlsx', 'confirmed');
[d, t] = getTimelineFromSheet('./data/female.xlsx', 'deaths');

CFR = cfr(d, c);
plot(t, CFR, '-xr'); hold on
xlabel('time (days)');

% carlo's version
p = 47e6;
carlo = (d .* (p + c)) ./ (c .* (p + d));

plot(t, carlo, '--b');

legend([string('real'), string('approximation')]);
xlabel('time (days)');

title('CFR during Sars-CoV-2 in Spain');
