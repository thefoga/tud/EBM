clearvars; close all; clc;

addpath('../common/models/');
addpath('../common/models/FNH/');
addpath('../common/models/FNH/model');
addpath('../common/models/FNH/sim');
addpath('../common/models/FNH/fitting');
addpath('../common/stats/');
addpath('../common/math/');
addpath('../common/epidemology/');
addpath('../common/utils');
addpath('./utils');
addpath('./scenarios');

sheetNames = [...
  string('confirmed'), ......
  string('hospitalized'), ......
  string('ICU'), ......
  string('deaths')
];

fig = figure;
getSubPlot = @(i) subplot(2, 2, i);

for i=1:numel(sheetNames) % for each sheet
  getSubPlot(i);

  sheetName = sheetNames(i);
  [y, t] = getTimelineFromSheet('./data/female.xlsx', sheetName);
  plotStats(y, t, sheetName);
end

sgtitle('Sars-CoV-2 in Spain');
