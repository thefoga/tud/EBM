function [] = plotStats(y, t, titleName)
  yyaxis left

  % real values
  plot(t, y, '-xr'); hold on

  % exponential MA filtered
  alpha = 0.5;
  f = filter(alpha, [1, alpha - 1], y);
  plot(t, f, '--b'); hold on

  yyaxis right

  % delta
  d = diff(y); % d(i) = y(i) - y(i - 1) = backward difference
  plot(t(2:end), d, '-xg'); hold on

  % exponential MA filtered on delta
  f = filter(alpha, [1, alpha - 1], d);
  plot(t(2:end), f, '--b'); hold on
  
  labels = [string('totals'), string('exponential MA'), string('delta'), string('exponential MA')];

  legend(labels);
  xlabel('time (days)');
  title(titleName);
end
