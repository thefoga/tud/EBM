function [table] = getSheetData(dataFile, sheetName)
parseTime = @(raw) datetime(raw, 'InputFormat', 'dd MMM');

%% setup the import options and import the data

% time
timeStart = parseTime('24 Mar');
timeEnd = parseTime('18 May');
timeAvailable = [timeStart:1:timeEnd];

% labels
labels = datestr(timeAvailable);
labels = ['age', string(labels)'];

% # of columns
opts = spreadsheetImportOptions('NumVariables', numel(labels));
opts.VariableNames = labels;

variableTypes = [string('string')]; % age
for v=2:numel(labels)
  variableTypes = [variableTypes, string('double')];
end
opts.VariableTypes = variableTypes;

% specify file level properties
opts.DataRange = '2:11'; % start cell
opts.PreserveVariableNames = true;

% import the data
opts.Sheet = sheetName;
tmp = readtable(dataFile, opts);

% transpose (time should be vertical)
a = table2array(tmp);
table = array2table(a.');

table.Properties.VariableNames = string(tmp.age)';
table(1, :) = []; % remove first row (labels)

time = tmp.Properties.VariableNames(2:end);
time = datetime(time, 'InputFormat', 'dd-MMM-yyyy');

table.time = time';
end
