function [y, t] = getTimelineFromSheet(fileName, sheetName)
  table = getSheetData(fileName, sheetName);
  timeRef = table.('time');

  totals = zeros(height(table), 1);
  for j=1:numel(table.Properties.VariableNames) % for each variable in data
    varName = string(table.Properties.VariableNames(j));
    if varName ~= 'time'
      values = str2double(table.(varName));
      totals = totals + values;
    end
  end

  t = timeRef;
  y = totals;
end
